EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:TEM-HUM-AM2302_4P-25.0X15.0MM_
LIBS:ESP8266
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "TFG-NODE 4, 5, i 6"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ESP-01v090 U?
U 1 1 5B79AF87
P 5550 3700
F 0 "U?" H 5550 3600 50  0000 C CNN
F 1 "ESP-01v090" H 5550 3800 50  0000 C CNN
F 2 "" H 5550 3700 50  0001 C CNN
F 3 "" H 5550 3700 50  0001 C CNN
	1    5550 3700
	1    0    0    -1  
$EndComp
$Comp
L TEM-HUM-AM2302(4P-25.0X15.0MM) U?
U 1 1 5B79AFDF
P 7700 3700
F 0 "U?" H 7550 3900 50  0000 L BNN
F 1 "AM2320" H 7550 3450 50  0000 L BNN
F 2 "SNR4-2.54-25.0X15.0X7.0MM" H 7700 3700 50  0001 L BNN
F 3 "MCM Electronics" H 7700 3700 50  0001 L BNN
F 4 "None" H 7700 3700 50  0001 L BNN "Field4"
F 5 "AM2302" H 7700 3700 50  0001 L BNN "Field5"
F 6 "Unavailable" H 7700 3700 50  0001 L BNN "Field6"
F 7 "DHT22 Temp / Humidity Sensor DHT22 Temp / Humidity Sensor" H 7700 3700 50  0001 L BNN "Field7"
F 8 "None" H 7700 3700 50  0001 L BNN "Field8"
	1    7700 3700
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR?
U 1 1 5B79B0BD
P 6500 3550
F 0 "#PWR?" H 6500 3400 50  0001 C CNN
F 1 "VSS" H 6500 3700 50  0000 C CNN
F 2 "" H 6500 3550 50  0001 C CNN
F 3 "" H 6500 3550 50  0001 C CNN
	1    6500 3550
	0    1    1    0   
$EndComp
$Comp
L VSS #PWR?
U 1 1 5B79B0D5
P 7400 3850
F 0 "#PWR?" H 7400 3700 50  0001 C CNN
F 1 "VSS" H 7400 4000 50  0000 C CNN
F 2 "" H 7400 3850 50  0001 C CNN
F 3 "" H 7400 3850 50  0001 C CNN
	1    7400 3850
	-1   0    0    1   
$EndComp
$Comp
L +3V3 #PWR?
U 1 1 5B79B0E6
P 7400 3550
F 0 "#PWR?" H 7400 3400 50  0001 C CNN
F 1 "+3V3" H 7400 3690 50  0000 C CNN
F 2 "" H 7400 3550 50  0001 C CNN
F 3 "" H 7400 3550 50  0001 C CNN
	1    7400 3550
	1    0    0    -1  
$EndComp
$Comp
L +3V3 #PWR?
U 1 1 5B79B0FE
P 4250 3850
F 0 "#PWR?" H 4250 3700 50  0001 C CNN
F 1 "+3V3" H 4250 3990 50  0000 C CNN
F 2 "" H 4250 3850 50  0001 C CNN
F 3 "" H 4250 3850 50  0001 C CNN
	1    4250 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3850 4600 3850
Wire Wire Line
	6500 3650 7400 3650
$EndSCHEMATC
