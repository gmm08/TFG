# TFG

In this project, a low-cost, structural health monitoring system for historical buildings has been developed. In order to do this the appropriate sensors have been chosen, and the needed hardware and software have been developed to ensure data arrives to the end-user. An Internet connected network structure has been implemented, together with a browser accessible user interface, allows us to present the data in a user-friendly way. This project follows the open-source philosophy, and as such every file and documentation are public and freely accessible to anybody that so wishes.
