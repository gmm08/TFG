#include <SPI.h>
#include <SD.h>
#include "painlessMesh.h"
#include <DS3231.h>
#include <Wire.h>

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

DS3231 Clock;
bool h12;
bool PM;


const int chipSelect = D0;

painlessMesh  mesh;

uint16_t Fletcher16( char *data, int count )
{
  uint16_t sum1 = 0;
  uint16_t sum2 = 0;
  int index;

  for ( index = 0; index < count; ++index )
  {
    sum1 = (sum1 + data[index]) % 255;
    sum2 = (sum2 + sum1) % 255;
  }

  return (sum2 << 8) | sum1;
}

void receivedCallback( uint32_t from, String &msg ) {


  String t = String(Clock.getHour(h12, PM), DEC) + ':' + String(Clock.getMinute(), DEC) + ':' + String(Clock.getSecond(), DEC);



  String msg_sd = t + ',' + msg;

  //  File dataFile = SD.open("datalog.csv", FILE_WRITE);
  //  if (dataFile) {
  //    dataFile.println(msg.c_str());
  //    dataFile.close();
  //  }

  char buff[64];
  memset(buff, NULL, 64);
  msg.toCharArray(buff, 64);
  uint8_t len = strlen(buff);



  uint16_t chksum = Fletcher16(buff, len);
  char msg_mqtt[100];

  msg_mqtt[0] = 0x01;
  msg_mqtt[1] = (from >> 24) & 0x000000ff;
  msg_mqtt[2] = (from >> 16) & 0x000000ff;
  msg_mqtt[3] = (from >> 8) & 0x000000ff;
  msg_mqtt[4] = from & 0x000000ff;
  msg_mqtt[5] = len;
  memcpy(&msg_mqtt[6], buff, len);
  msg_mqtt[6 + len] = (chksum >> 8) & 0x00ff;
  msg_mqtt[7 + len] = chksum & 0x00ff;
  msg_mqtt[8 + len] = 0x04;



  Serial.write(msg_mqtt, len + 9);

  Serial.flush();

}

void newConnectionCallback(uint32_t nodeId) {
}

void changedConnectionCallback() {
}

void nodeTimeAdjustedCallback(int32_t offset) {
}

void setup() {
  Serial.begin(115200);
  Wire.begin();

  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);


  if (!SD.begin(chipSelect)) {

  }
}
void loop() {
  mesh.update();
}


