#include <ESP8266WiFi.h>
#include <PubSubClient.h>


const char* ssid = "arris54g";
const char* password = "";

const char* mqtt_server = "monitoratge.duckdns.org";
const int port = 1052;

uint8_t incomingByte, numOfBytes, state;
char incoming[100];
bool recieving;
const char ack[] = {0x01, 0x06, 0x04};
const char nack[] = {0x01, 0x15, 0x04};


typedef struct message {
  uint8_t dlen;
  uint32_t id;
  char data[64];
  char topic[16];
  uint16_t chksum;
} message_t;

message_t message;

WiFiClient espClient;
PubSubClient client(espClient);


void MQTT_Callback(char* topic, byte* payload, unsigned int length) {

}

uint16_t Fletcher16( char *data, int count )
{
  uint16_t sum1 = 0;
  uint16_t sum2 = 0;
  int index;

  for ( index = 0; index < count; ++index )
  {
    sum1 = (sum1 + data[index]) % 255;
    sum2 = (sum2 + sum1) % 255;
  }

  return (sum2 << 8) | sum1;
}

bool parseMsg(char msg[]) {



  bool ret = false;

  message.id = (msg[0] << 24) + (msg[1] << 16) + (msg[2] << 8) + msg[3];
  message.dlen = msg[4];
  memmove(message.data, &msg[5], message.dlen);


  message.chksum = (msg[5 + message.dlen] << 8) + msg[6 + message.dlen];
  if (Fletcher16(message.data, message.dlen) == message.chksum) ret = true;
  message.data[message.dlen] = NULL;
  sprintf (message.topic, "%d", message.id);
  //sprintf (message.topic, "prova");
  return ret;
}

void getmsg() {

  if (Serial.available() > 0) {

    if (recieving) {

      memset(incoming, 0, 64);
      numOfBytes = Serial.readBytesUntil(0x04, incoming, 64);
      recieving = 0;

      if (numOfBytes > 0) {
        state = 1;
      }

    }
    else {
      incomingByte = Serial.read();
      if (incomingByte == 0x01) {
        recieving = 1;
      }
    }
  }

}

void reconnect() {
  while (!client.connected()) {
    if (client.connect("ESP8266Client")) {
      //      client.subscribe("inTopic");
    } else {
      delay(5000);
    }
  }
}

void setup() {


  Serial.begin(115200);
  recieving = 0;
  incomingByte = 0;
  state = 3;

  client.setServer(mqtt_server, port);
  client.setCallback(MQTT_Callback);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

}

void loop() {

  if (!client.connected()) {
    reconnect();
    state = 3;
  }
  else {

  }
  client.loop();

  switch (state) {

    case 0:
      getmsg();
      break;

    case 1:
      if (parseMsg(incoming)) {
        state = 2;
        Serial.write(ack, 3);
        Serial.flush();
      }
      else {
        state = 0;
        Serial.write(nack, 3);
        Serial.flush();
      }

      break;

    case 2:
      client.publish(message.topic, message.data);
      state = 0;
      break;

    case 3:
      client.publish("prova", "Connected");
      state = 0;
      break;

    default:
      break;
  }




}
