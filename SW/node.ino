

#include "painlessMesh.h"
#include "DHT.h"


#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

void sendMessage() ; // Prototype so PlatformIO doesn't complain

DHT dhtB(1, DHT22);

painlessMesh  mesh;
Task taskSendMessage( TASK_SECOND * 300 , TASK_FOREVER, &sendMessage );



//void receivedCallback( uint32_t from, String &msg ) {
////  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
//}
//
//void newConnectionCallback(uint32_t nodeId) {
////  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
//}
//
//void changedConnectionCallback() {
////  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
//}
//
//void nodeTimeAdjustedCallback(int32_t offset) {
////  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
//}


void setup() {
  //Serial.begin(115200);


  dhtB.begin();
 


  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
//  mesh.setDebugMsgTypes(  );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT );
//  mesh.onReceive(&receivedCallback);
//  mesh.onNewConnection(&newConnectionCallback);
//  mesh.onChangedConnections(&changedConnectionCallback);
//  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  mesh.scheduler.addTask( taskSendMessage );
  taskSendMessage.enable() ;
}

void loop() {
  mesh.update();
}


void sendMessage() {

  String msg;


  float hB = dhtB.readHumidity();
  float tB = dhtB.readTemperature();


  msg = String(tB, 1) + "," + String(hB, 1);
  mesh.sendBroadcast( msg );

  

}





