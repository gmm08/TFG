#include "DHT.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>
#include "painlessMesh.h"
#include <math.h>

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

#define LM_SIZE 100


inline double to_degrees(double radians) {
  return radians * (180.0 / M_PI);
}

void runningAverage(double *pitch, double *roll) {

  static double LM_p[LM_SIZE], LM_r[LM_SIZE];     // LastMeasurements
  static byte index = 0;
  static double sum_p = 0, sum_r = 0;
  static byte count = 0;

  // keep sum updated to improve speed.
  sum_p -= LM_p[index];
  sum_r -= LM_r[index];
  LM_p[index] = *pitch;
  LM_r[index] = *roll;
  sum_p += LM_p[index];
  sum_r += LM_r[index];
  index++;
  index = index % LM_SIZE;
  if (count < LM_SIZE) count++;

  *pitch = sum_p / count;
  *roll = sum_r / count;
}

Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);
DHT dhtB(D7, DHT22);
DHT dhtN(D5, DHT22);

void sendMessage() ; // Prototype so PlatformIO doesn't complain


painlessMesh  mesh;
Task taskSendMessage( TASK_SECOND * 1 , TASK_FOREVER, &sendMessage );

void receivedCallback( uint32_t from, String &msg ) {
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
}

void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}



void setup() {
  Serial.begin(115200);

  dhtB.begin();
  dhtN.begin();

  accel.begin();
  accel.setRange(ADXL345_RANGE_2_G);

  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  mesh.scheduler.addTask( taskSendMessage );
  taskSendMessage.enable() ;

  taskSendMessage.setInterval( TASK_SECOND * 5 );
}

void loop() {
  mesh.update();
}


void sendMessage() {

  String msg;
  double x, y, z;

  /* Get a new sensor event */
  sensors_event_t event;
  accel.getEvent(&event);

  x = event.acceleration.x;
  y = event.acceleration.y;
  z = event.acceleration.z;

  double angle_yz = to_degrees(atan2(z, -y));
  double angle_xy = to_degrees(atan2(-x, -y));
  runningAverage(&angle_yz, &angle_xy);

  Serial.print(angle_yz);
  Serial.print(',');
  Serial.println(angle_xy);

  static int cont;

  cont++;

  if (cont >= 60) {
    cont = 0;
    float hB = dhtB.readHumidity();
    float tB = dhtB.readTemperature();
    float hN = dhtN.readHumidity();
    float tN = dhtN.readTemperature();


    msg = String(tB, 1) + "," + String(tN, 1) + "," + String(hB, 1) + "," + String(hN, 1) + "," + String(angle_yz) + "," + String(angle_xy);

    mesh.sendBroadcast( msg );
  }







}





