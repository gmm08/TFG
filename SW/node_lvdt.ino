

#include "painlessMesh.h"
#include "DHT.h"
#include <Wire.h>
#include <Adafruit_ADS1015.h>


#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

#define LM_SIZE 120

Adafruit_ADS1115 ads(0x48);
DHT dhtB(D3, DHT22);
DHT dhtN(D4, DHT22);

void sendMessage() ;
painlessMesh  mesh;
Task taskSendMessage( TASK_SECOND * 5 , TASK_FOREVER, &sendMessage );

void receivedCallback( uint32_t from, String &msg ) {
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
}

void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

long runningAverage(int16_t M) {

  static int16_t LM[LM_SIZE];      // LastMeasurements
  static byte index = 0;
  static long sum = 0;
  static byte count = 0;

  // keep sum updated to improve speed.
  sum -= LM[index];
  LM[index] = M;
  sum += LM[index];
  index++;
  index = index % LM_SIZE;
  if (count < LM_SIZE) count++;

  return sum / count;
}



void setup() {
  Serial.begin(115200);

  dhtB.begin();
  dhtN.begin();
  Wire.begin(D1, D2);

  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  ads.setGain(GAIN_ONE);
  ads.begin();


  mesh.scheduler.addTask( taskSendMessage );
  taskSendMessage.enable();

}

void loop() {
  mesh.update();
}


void sendMessage() {



  int16_t lvdt;
  //lvdt = ads.readADC_Differential_0_1();
  lvdt = ads.readADC_SingleEnded(0) - 22530;
  long lvdt_avg = runningAverage(lvdt);
  static int cont;

  cont++;

  if (cont >= 60) {
    cont = 0;
    float hB = dhtB.readHumidity();
    float tB = dhtB.readTemperature();
    float hN = dhtN.readHumidity();
    float tN = dhtN.readTemperature();


    String msg = "";



    msg = String(tB, 1) + "," + String(tN, 1) + "," + String(hB, 1) + "," + String(hN, 1) + "," + String(lvdt_avg);
    //msg = String(lvdt) + ',' + String(lvdt_avg);
    Serial.println(msg);

    mesh.sendBroadcast( msg );



  }




}





