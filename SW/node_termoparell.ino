

#include "painlessMesh.h"
#include "Ads1118.h"
#include "DHT.h"
#include <SPI.h>

#define DEBUG

#define CS D4
#define SEEBECK 41

#define   MESH_PREFIX     "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

void sendMessage() ; // Prototype so PlatformIO doesn't complain

DHT dhtB(D1, DHT22);
DHT dhtN(D2, DHT22);
Ads1118 ads1118(CS); // instantiate an instance of class Ads1118

painlessMesh  mesh;
Task taskSendMessage( TASK_SECOND * 1 , TASK_FOREVER, &sendMessage );

void receivedCallback( uint32_t from, String &msg ) {
  Serial.printf("startHere: Received from %u msg=%s\n", from, msg.c_str());
}

void newConnectionCallback(uint32_t nodeId) {
  Serial.printf("--> startHere: New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
}

void nodeTimeAdjustedCallback(int32_t offset) {
  Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

double readTref() {
  ads1118.readTemp();
  delay(50);
  return ads1118.readTemp();
}

double readADC(word conf) {

  ads1118.self_test();
  ads1118.setGain(0b111);
  ads1118.adsReadRaw(conf);
  delay(50);
  int16_t tcraw = ads1118.adsReadRaw(conf);
  return tcraw * 7.8125;

}

void readTemps(double t[], double* tref) {

  *tref = readTref();
  t[0] = readADC(ads1118.DIF01);
  t[0] = (t[0] / SEEBECK) + *tref;
  t[1] = readADC(ads1118.DIF23);
  t[1] = (t[1] / SEEBECK) + *tref;

}

void setup() {
  Serial.begin(115200);
  ads1118.begin();
  dhtB.begin();
  dhtN.begin();
  //ads1118.setGain(0b111);

  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

  mesh.init( MESH_PREFIX, MESH_PASSWORD, MESH_PORT );
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

  mesh.scheduler.addTask( taskSendMessage );
  taskSendMessage.enable() ;
  taskSendMessage.setInterval( TASK_SECOND * 300 );
}

void loop() {
  mesh.update();
}


void sendMessage() {

  String msg;

  //mesh.sendBroadcast( msg );

  double temp[2];
  double tref;
  readTemps(temp, &tref);

  float hB = dhtB.readHumidity();
  float tB = dhtB.readTemperature();
  float hN = dhtN.readHumidity();
  float tN = dhtN.readTemperature();

  msg = String(tB, 1) + "," + String(tN, 1) + "," + String(hB, 1) + "," + String(hN, 1) + "," + String(temp[0], 2) + "," + String(temp[1]-6.5, 2);
  mesh.sendBroadcast( msg );


#ifdef DEBUG
  //msg = "Temp1:" + String(temp[0], 2) + ", Temp2:" + String(temp[1], 2) + ", Tref:" + String(tref, 2) + ", TempB:" + String(tB, 1) + ", TempN:" + String(tN, 1) + ", HumB:" + String(hB, 1) + ", HumN:" + String(hN, 1) + "\n";
  Serial.println(msg);
  Serial.println();
#endif



}





